using System.Collections;
using UnityEngine;

namespace Game
{
    public class Bomb : MonoBehaviour
    {
        [SerializeField] private float _delay;
        [SerializeField] private GameObject _explosionZonePrefab;

        private float _gridStep = 1.25f;

        private void Awake()
        {
            StartCoroutine(ExplosionDelay());
        }

        IEnumerator ExplosionDelay()
        {
            yield return new WaitForSeconds(_delay);
            Explosion();
            Destroy(gameObject);
        }

        private void Explosion()
        {
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x, 0.2f, transform.position.z), Quaternion.identity);
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x + _gridStep, 0.2f, transform.position.z), Quaternion.identity);
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x - _gridStep, 0.2f, transform.position.z), Quaternion.identity);
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x, 0.2f, transform.position.z + _gridStep), Quaternion.identity);
            Instantiate(_explosionZonePrefab, new Vector3(transform.position.x, 0.2f, transform.position.z - _gridStep), Quaternion.identity);
        }
    }
}
