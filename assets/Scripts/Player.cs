﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    [RequireComponent(typeof(Movement))]
    public class Player : MonoBehaviour
    {
        public bool IsAlive { get; private set; } = true;
        public bool HasKey { get; private set; } = false;

        private Movement _movement;
        [SerializeField] private GameObject _bombPrefab;

        private void Awake()
        {
            _movement = GetComponent<Movement>();
        }

        private void Start()
        {
            Enable();
        }

        private void Update()
        {
            bool _bombPlanted = FindObjectOfType<Bomb>();
            if (!_bombPlanted && SceneManager.GetActiveScene().buildIndex == 2 && Input.GetKeyDown(KeyCode.Space))
            {
                Instantiate(_bombPrefab, transform.position, Quaternion.identity);
            }
        }

        public void Enable()
        {
            _movement.enabled = true;
        }

        public void Disable()
        {
            _movement.enabled = false;
        }

        public void Kill()
        {
            IsAlive = false;
        }

        public void PickUpKey()
        {
            HasKey = true;
        }
    }
}