using UnityEngine;

namespace Game
{
    public class Portal : MonoBehaviour
    {
        [SerializeField] private GameObject _portalExit;

        private void OnTriggerEnter(Collider other)
        {
            Vector2 _exit = new Vector2(_portalExit.transform.position.x, _portalExit.transform.position.z);
            Vector3 _exitPosition = new Vector3(_exit.x, other.transform.position.y, _exit.y);

            other.gameObject.transform.position = _exitPosition;
        }
    }
}